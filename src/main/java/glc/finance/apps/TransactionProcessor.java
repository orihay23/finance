package glc.finance.apps;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.google.common.collect.ArrayListMultimap;

public class TransactionProcessor {

	private static final DateTimeFormatter dateFormatter = DateTimeFormatter
			.ofPattern("d[d]-MMM-uu");

	public static void main(String[] args) throws FileNotFoundException {
		new TransactionProcessor().parseCsv(new File("src/main/resources",
				"Sample Account Transactions - Account Transactions.txt"));
	}

	public List<Donor> parseCsv(File file) throws FileNotFoundException {
		ArrayListMultimap<String, Transaction> transactionMap = ArrayListMultimap
				.create();
		InputStream is = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		List<Transaction> transactions = br.lines().skip(7)
		// .substream(1)
				.map(mapToTransaction).collect(Collectors.toList());
		// .filter(person -> person.beforeEmpty())
		for (Transaction t : transactions) {
			System.out.println(t);
			transactionMap.put(t.getDonor(), t);
		}
		ArrayList<Donor> donorList = new ArrayList<Donor>();
		for (String name : transactionMap.keys()) {
			donorList.add(new Donor(name, transactionMap.get(name)));
		}
		return donorList;
	}

	public Function<String, Transaction> mapToTransaction = (line) -> {
		String[] p = line.split("\\t");
		String fund = getFund(p[1]);
		String currency = getCurrency(p[1], p[3]);
		String checkNumber = "";
		if (currency.equals("Check")) {
			int indexOf = p[1].indexOf("CHK");
			String substring = p[1].substring(indexOf);
			int space = substring.indexOf(" ");
			checkNumber = substring.substring(0, space + 1);
		}
		return new Transaction(LocalDate.parse(p[0], dateFormatter), p[2],
				fund, currency, checkNumber, new BigDecimal(p[5]));
	};

	public static String getFund(String description) {
		String fund = null;
		if (description.contains("GLCE")) {
			fund = "GLCE";;
		} else if (description.contains("Peru") || description.contains("Missions")) {
			fund = "Missions";
		} else if (description.contains("SSM")) {
			fund = "SSM";
		} else if (description.contains("LGT")) {
			fund = "LGT";
		} else {
			fund = "";
		}
		return fund;
	}

	private String getCurrency(String description, String reference) {
		String currency = null;
		if (description.contains("CHK")) {
			currency = "Check";
		} else if (reference.contains("N4G")) {
			currency = "Online";
		} else {
			currency = "Cash";
		}
		return currency;
	}

}
