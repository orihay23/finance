package glc.finance.apps;

import java.math.BigInteger;
import java.text.NumberFormat;
import java.util.List;

public class Donor {
  private String name;
  private List<Transaction> transactions;
public static void main(String[] args) {
  NumberFormat currencyInstance = NumberFormat.getCurrencyInstance();

  System.out.println(String.format(currencyInstance.format(new BigInteger("101"))));
}
  public Donor(String name, List<Transaction> transactions) {
    this.name = name;
    this.transactions = transactions;
  }

  public String getName() {
    return name;
  }

  public List<Transaction> getTransactions() {
    return transactions;
  }
}
