package glc.finance.apps;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Transaction {
	private final LocalDate date;
	private final String donor;
	private final String fund;
	private final String currency;
	private final String checkNumber;
	private final BigDecimal donationAmt;

	public Transaction(LocalDate date, String donor, String fund,
			String currency, String checkNumber, BigDecimal donationAmt) {
		this.date = date;
		this.donor = donor;
		this.fund = fund;
		this.currency = currency;
		this.checkNumber = checkNumber;
		this.donationAmt = donationAmt;
	}

	public String getFund() {
		return fund;
	}

	public String getCurrency() {
		return currency;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public String toString() {
		return date + ": " + donor + " " + fund + " " + currency + " "
				+ checkNumber + " " + donationAmt;
	}

	public LocalDate getDate() {
		return date;
	}

	public String getDonor() {
		return donor;
	}

	public BigDecimal getDonationAmt() {
		return donationAmt;
	}
}
