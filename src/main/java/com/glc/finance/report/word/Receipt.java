package com.glc.finance.report.word;

import glc.finance.apps.Donor;
import glc.finance.apps.Transaction;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;

import org.apache.poi.xwpf.usermodel.BodyElementType;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

import com.glc.finance.report.html.AddressBlock;

public class Receipt {
	private final AddressBlock block;
	private final Donor donor;

	public Receipt(AddressBlock block, Donor donor) {
		this.block = block;
		this.donor = donor;
	}

	public XWPFDocument document() throws IOException {
		XWPFDocument doc = new XWPFDocument();
		FileInputStream is = new FileInputStream(new File("sample.docx"));
		XWPFDocument read = new XWPFDocument(is);

		doc = read;
		// read.close();
		XWPFParagraph nameParagraph = doc.createParagraph();
		nameParagraph.setSpacingAfter(0);
		XWPFRun createRun = nameParagraph.createRun();
		createRun.setText(block.getName());
		XWPFParagraph addressParagraph = doc.createParagraph();
		addressParagraph.setSpacingAfter(0);
		addressParagraph.createRun().setText(block.getStreet());
		XWPFParagraph cityStateParagraph = doc.createParagraph();
		cityStateParagraph.setSpacingAfter(0);
		cityStateParagraph.createRun().setText(block.getCityStateZip());
		doc.createParagraph();
		XWPFParagraph officialParagraph = doc.createParagraph();
		officialParagraph.setSpacingAfter(0);
		officialParagraph.createRun().setText(
				"This is an official receipt for income tax purposes.");
		doc.removeBodyElement(0);
		doc.removeBodyElement(1);
		doc.removeBodyElement(2);
		doc.removeBodyElement(3);
		doc.removeBodyElement(4);
		List<IBodyElement> bodyElements = doc.getBodyElements();
		doc.setParagraph(nameParagraph, 5);
		doc.setParagraph(addressParagraph, 6);
		doc.setParagraph(cityStateParagraph, 7);
		doc.setParagraph(officialParagraph, 9);
		boolean afterTable = false;
		for (int i = 0; i < bodyElements.size(); i++) {
			if (afterTable) {
				doc.removeBodyElement(i);
			} else {
				if (bodyElements.get(i).getElementType() == BodyElementType.TABLE) {
					afterTable = true;
				}
			}
		}
		doc.removeBodyElement(16);
		doc.removeBodyElement(14);
		doc.removeBodyElement(13);
		doc.removeBodyElement(12);
		doc.removeBodyElement(15);
		doc.removeBodyElement(11);
		XWPFTable xwpfTable = doc.getTables().get(0);
		for (Transaction t : donor.getTransactions()) {
			XWPFTableRow row = xwpfTable.createRow();
			row.getCell(0).setText(t.getDate().toString());
			row.getCell(1).setText(t.getFund());
			row.getCell(2).setText(NumberFormat.getCurrencyInstance().format(t.getDonationAmt()));
			row.getCell(3).setText("Yes");
			row.getCell(4).setText(t.getCurrency());
			row.getCell(5).setText(t.getCheckNumber());
			row.getCell(6).setText(NumberFormat.getCurrencyInstance().format(t.getDonationAmt()));
		}
		return doc;
	}

	public static void main(String[] args) throws IOException {
		String name = "John Yahiro";
		String address = "6163 Quiet Times";
		String cityState = "Columbia, MD 21045";
		XWPFDocument doc = new XWPFDocument();
		FileInputStream is = new FileInputStream(new File("sample.docx"));
		XWPFDocument read = new XWPFDocument(is);

		doc = read;
		// read.close();
		XWPFParagraph nameParagraph = doc.createParagraph();
		nameParagraph.setSpacingAfter(0);
		XWPFRun createRun = nameParagraph.createRun();
		createRun.setText(name);
		XWPFParagraph addressParagraph = doc.createParagraph();
		addressParagraph.setSpacingAfter(0);
		addressParagraph.createRun().setText(address);
		XWPFParagraph cityStateParagraph = doc.createParagraph();
		cityStateParagraph.setSpacingAfter(0);
		cityStateParagraph.createRun().setText(cityState);
		doc.createParagraph();
		XWPFParagraph officialParagraph = doc.createParagraph();
		officialParagraph.setSpacingAfter(0);
		officialParagraph.createRun().setText(
				"This is an official receipt for income tax purposes.");
		doc.removeBodyElement(0);
		doc.removeBodyElement(1);
		doc.removeBodyElement(2);
		doc.removeBodyElement(3);
		doc.removeBodyElement(4);
		List<IBodyElement> bodyElements = doc.getBodyElements();
		doc.setParagraph(nameParagraph, 5);
		doc.setParagraph(addressParagraph, 6);
		doc.setParagraph(cityStateParagraph, 7);
		doc.setParagraph(officialParagraph, 9);
		boolean afterTable = false;
		for (int i = 0; i < bodyElements.size(); i++) {
			if (afterTable) {
				doc.removeBodyElement(i);
			} else {
				if (bodyElements.get(i).getElementType() == BodyElementType.TABLE) {
					afterTable = true;
				}
			}
		}
		doc.removeBodyElement(16);
		doc.removeBodyElement(14);
		doc.removeBodyElement(13);
		doc.removeBodyElement(12);
		doc.removeBodyElement(15);
		doc.removeBodyElement(11);
		XWPFTable xwpfTable = doc.getTables().get(0);
		XWPFTableRow row = xwpfTable.createRow();
		FileOutputStream out = new FileOutputStream(new File("out.docx"));
		doc.write(out);
		// and then use apache fop for changing to pdf?
	}
}
