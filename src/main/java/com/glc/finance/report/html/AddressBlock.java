package com.glc.finance.report.html;

public class AddressBlock {
    private final String name;
    private final String street;
    private final String cityStateZip;

    public AddressBlock(String name, String street, String cityStateZip) {
        this.name = name;
        this.street = street;
        this.cityStateZip = cityStateZip;
    }

    public String getName() {
		return name;
	}

	public String getStreet() {
		return street;
	}

	public String getCityStateZip() {
		return cityStateZip;
	}

	public String asHtmlDivWithId(Id htmlId, Indentation indentation) {
        Indentation deeper = indentation.indent();
        return "\n"
                + indentation + "<div id='" + htmlId + "'>\n"
                + deeper + addressLineWithIdAndValue(Id.DONER_ADDRESS_NAME_ID, name)
                + deeper + addressLineWithIdAndValue(Id.DONER_ADDRESS_STREET_ID, street)
                + deeper + addressLineWithIdAndValue(Id.DONER_ADDRESS_CITYSTATEZIP_ID, cityStateZip)
                + indentation + "</div>\n";
    }

    private String addressLineWithIdAndValue(Id htmlId, String lineContents) {
        return String.format("<div class='addressBlockLine' id='%s'>%s</div>\n", htmlId, lineContents);
    }
}
