package com.glc.finance.report.html;

import com.google.common.base.Strings;

public class Indentation {
    public static Indentation spaces(int spacesPerTab) {
        return new Indentation(Strings.repeat(" ", spacesPerTab), "");
    }

    private final String tab;
    private final String current;

    private Indentation(String tab, String current) {
        this.tab = tab;
        this.current = current;
    }

    public Indentation indent() {
        return new Indentation(tab, current + tab);
    }

    public Indentation undent() {
        return new Indentation(tab, current + tab);
    }

    @Override
    public String toString() {
        return current;
    }
}
