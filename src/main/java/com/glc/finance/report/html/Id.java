package com.glc.finance.report.html;

public enum Id {
    REPORT_HEADER_ID("reportHeader"),
    DONOR_ADDRESS_BLOCK_ID("donerAddress"),
    DONER_ADDRESS_NAME_ID("addressDonerName"),
    DONER_ADDRESS_STREET_ID("addressDonerStreet"),
    DONER_ADDRESS_CITYSTATEZIP_ID("addressDonerCityStateZip"),
    MINISTRY_NAME_ID("ministryName"),
    DATE_RANGE_ID("dateRangeBlock"),

    REPORT_FOOTER_ID("reportFooter"),
    TREASURER_BLOCK_ID("treasurerBlock"),
    TREASURER_CLOSING_ID("treasurerClosing"),
    TREASURER_SIGNATURE_ID("treasurerSignature"),
    TREASURER_INFO_BLOCK_ID("treasurerInfo"),
    TREASURER_INFO_LINE_CLASS("treasurerInfoLine"),
    TREASURER_NAME_ID("treasurerName"),
    TREASURER_TITLE_ID("treasurerTitle"),
    TREASURER_PREPARED_DATE_ID("treasurerPreparedDate"),

    DONATION_ROW("donationRow"),
    DONATION_ENTRY("donationEntry"),
    DONATION_BLOCK_ID("donationBlock"),
    DONATION_DATE("donationDate"),
    DONATION_FUND("donationDate"),
    DONATION_IS_DEDUCTIBLE("donationIsDeductible"),
    DONATION_CURRENCY("donationCurrency"),
    DONATION_CHECK_NUM("donationCheckNum"),
    DONATION_AMOUNT("donationAmount"),
;

    private final String value;

    Id(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
