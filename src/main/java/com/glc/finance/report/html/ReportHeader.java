package com.glc.finance.report.html;

import java.time.LocalDate;

import static java.time.format.DateTimeFormatter.ISO_DATE;

public class ReportHeader {
    private final String ministryName;
    private final AddressBlock donor;
    private final LocalDate from;
    private final LocalDate to;

    public ReportHeader(String ministryName, AddressBlock donor, LocalDate from, LocalDate to) {
        this.ministryName = ministryName;
        this.donor = donor;
        this.from = from;
        this.to = to;
    }

    public String asHtmlDivWithId(Id htmlId, Indentation tab) {
        String format = tab + "<div id='%s'>\n"
                + "%s\n"
                + tab + "</div>\n";
        return String.format(format, htmlId, content(tab.indent()));
    }

    private String content(Indentation tab) {
        return ""
                + ministry(tab)
                + dates(tab)
                + donor.asHtmlDivWithId(Id.DONOR_ADDRESS_BLOCK_ID, tab);
    }

    private String ministry(Indentation tab) {
        return tab + String.format("<div id='%s'>%s</div>\n", Id.MINISTRY_NAME_ID, ministryName);
    }

    private String dates(Indentation tab) {
        String dateRange = String.format("Records from %s to %s", from.format(ISO_DATE), to.format(ISO_DATE));
        return tab + String.format("<div id='%s'>%s</div>\n", Id.DATE_RANGE_ID, dateRange);
    }
}
