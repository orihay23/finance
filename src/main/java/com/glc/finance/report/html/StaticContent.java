package com.glc.finance.report.html;

public final class StaticContent {
    static final String TREASURER_CLOSING_REMARKS = "Thank you for your generous support,";
    static final String TREASURER_SIGNATURE_IMAGE_PATH = "treasurerSignature.png";

    private StaticContent() {
    }
}
