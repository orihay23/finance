package com.glc.finance.report.html;

import glc.finance.apps.Donor;
import glc.finance.apps.Transaction;

import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;

public class DonationsBlock {
  private static final NumberFormat CURRENCY = NumberFormat.getCurrencyInstance();

  private final Donor donor;

  public DonationsBlock(Donor donor) {
    this.donor = donor;
  }

  public String asHtmlDiv(Indentation tab) {
    Indentation deeper = tab.indent();
    return "\n"
        + tab + "<div id='" + Id.DONATION_BLOCK_ID + "'>\n"
        + deeper + "<table>" + "\n"
        + tableHeader(deeper)
        + rows(deeper)
        + deeper + "</table>\n"
        + tab + "</div>\n";
  }

  private String tableHeader(Indentation deeper) {
    deeper = deeper.indent();
    Indentation tabTwice = deeper.indent();
    return deeper + "<tr id='" + Id.DONATION_ROW + "'>\n"
        + tabTwice
        + "<th>Date</th>" + "\n"
        + tabTwice
        + "<th>Fund</th>" + "\n"
        + tabTwice
        + "<th>Ded.</th>" + "\n"
        + tabTwice
        + "<th>Currency Type</th>" + "\n"
        + tabTwice
        + "<th>Check #</th>" + "\n"
        + tabTwice
        + "<th>Amount</th>" + "\n"
        + deeper + "</tr>\n";
  }

  private String rows(Indentation deeper) {
    deeper = deeper.indent();
    Indentation tabTwice = deeper.indent();
    StringBuilder builder = new StringBuilder();
    for (Transaction t : donor.getTransactions()) {
      builder.append(
             deeper + "<tr id='" + Id.DONATION_ROW + "'>\n"
                 + tabTwice + donationEntryWithIdAndValue(Id.DONATION_DATE, t.getDate().format(DateTimeFormatter.ISO_DATE))
                 + tabTwice + donationEntryWithIdAndValue(Id.DONATION_FUND, "GLC-E")
                 + tabTwice + donationEntryWithIdAndValue(Id.DONATION_IS_DEDUCTIBLE, "Yes")
                 + tabTwice + donationEntryWithIdAndValue(Id.DONATION_CURRENCY, "Check")
                 + tabTwice + donationEntryWithIdAndValue(Id.DONATION_CHECK_NUM, "42")
                 + tabTwice + donationEntryWithIdAndValue(Id.DONATION_AMOUNT, CURRENCY.format(t.getDonationAmt()))
                 + deeper + "</tr>\n");
    }
    return builder.toString();
  }

  private String donationEntryWithIdAndValue(Id htmlId, String lineContents) {
    return String.format("<td class='%s' id='%s'>%s</td>\n",
                         Id.DONATION_ENTRY, htmlId, lineContents);
  }
}
