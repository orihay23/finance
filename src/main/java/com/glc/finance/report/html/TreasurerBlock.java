package com.glc.finance.report.html;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class TreasurerBlock {
    private final String name;
    private final String title;
    private final LocalDate preparedOn;

    public TreasurerBlock(String name, String title, LocalDate preparedOn) {
        this.name = name;
        this.title = title;
        this.preparedOn = preparedOn;
    }

    public String asHtmlDiv(Indentation tab) {
        Indentation deeper = tab.indent();
        return "\n"
                + tab + "<div id='" + Id.TREASURER_BLOCK_ID + "'>\n"
                + deeper + closing()
                + deeper + signature()
                + preparerInfo(deeper)
                + tab + "</div>\n";
    }

    private String signature() {
        return String.format("<div id='%s'><img src='%s' /></div>\n",
                Id.TREASURER_SIGNATURE_ID, StaticContent.TREASURER_SIGNATURE_IMAGE_PATH);
    }

    private String closing() {
        return String.format("<div id='%s'>%s</div>\n",
                Id.TREASURER_CLOSING_ID, StaticContent.TREASURER_CLOSING_REMARKS);
    }

    private String preparerInfo(Indentation tab) {
        Indentation deeper = tab.indent();
        return "\n"
                + tab + "<div id='" + Id.TREASURER_INFO_BLOCK_ID + "'>\n"
                + deeper + preparerLineWithIdAndValue(Id.TREASURER_NAME_ID, name)
                + deeper + preparerLineWithIdAndValue(Id.TREASURER_TITLE_ID, title)
                + deeper + preparerLineWithIdAndValue(Id.TREASURER_PREPARED_DATE_ID, preparedOn())
                + tab + "</div>\n";
    }

    private String preparerLineWithIdAndValue(Id htmlId, String lineContents) {
        return String.format("<div class='%s' id='%s'>%s</div>\n",
                Id.TREASURER_INFO_LINE_CLASS, htmlId, lineContents);
    }

    private String preparedOn() {
        return "Prepared " + preparedOn.format(DateTimeFormatter.ISO_DATE);
    }
}