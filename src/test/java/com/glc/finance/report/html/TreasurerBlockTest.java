package com.glc.finance.report.html;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

public class TreasurerBlockTest {
    @Test
    public void asHtmlDivFormatsAsExpected() throws Exception {
        String expected = "\n" +
                "  <div id='treasurerBlock'>\n" +
                "    <div id='treasurerClosing'>Thank you for your generous support,</div>\n" +
                "    <div id='treasurerSignature'><img src='treasurerSignature.png' /></div>\n" +
                "\n" +
                "    <div id='treasurerInfo'>\n" +
                "      <div class='treasurerInfoLine' id='treasurerName'>Scott</div>\n" +
                "      <div class='treasurerInfoLine' id='treasurerTitle'>Treasurer</div>\n" +
                "      <div class='treasurerInfoLine' id='treasurerPreparedDate'>Prepared 2015-01-01</div>\n" +
                "    </div>\n" +
                "  </div>\n";
        String toCheck = new TreasurerBlock("Scott", "Treasurer", LocalDate.of(2015, 1, 1))
                .asHtmlDiv(Indentation.spaces(2).indent());
        Assert.assertEquals(expected, toCheck);
    }
}
