package com.glc.finance.report.html;

import static org.junit.Assert.assertEquals;
import glc.finance.apps.Donor;
import glc.finance.apps.Transaction;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Test;

public class DonationsBlockTest {
	@Test
	public void htmlPrintsAsExpected() {
		String expected = ""
				+ "\n<div id='donerAddress'>\n"
				+ "  <div class='addressBlockLine' id='addressDonerName'>scott</div>\n"
				+ "  <div class='addressBlockLine' id='addressDonerStreet'>foo</div>\n"
				+ "  <div class='addressBlockLine' id='addressDonerCityStateZip'>bar, MD, 11111</div>\n"
				+ "</div>\n";
		System.out.println(sample().asHtmlDiv(Indentation.spaces(2)));
		assertEquals(expected, sample().asHtmlDiv(Indentation.spaces(2)));
	}

	public static DonationsBlock sample() {
		Transaction transaction = new Transaction(LocalDate.now(), "Sally",
				null, null, null, new BigDecimal("40.56"));
		ArrayList<Transaction> arrayList = new ArrayList<Transaction>();
		arrayList.add(transaction);
		Donor donor = new Donor("Sally", arrayList);
		return new DonationsBlock(donor);
	}
}
