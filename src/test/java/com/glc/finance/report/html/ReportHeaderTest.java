package com.glc.finance.report.html;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class ReportHeaderTest {
    @Test
    public void testName() throws Exception {
        String expected = "  <div id='reportHeader'>\n" +
                "    <div id='ministryName'>ministry</div>\n" +
                "    <div id='dateRangeBlock'>Records from 2015-01-01 to 2015-02-02</div>\n" +
                "\n" +
                "    <div id='donerAddress'>\n" +
                "      <div class='addressBlockLine' id='addressDonerName'>scott</div>\n" +
                "      <div class='addressBlockLine' id='addressDonerStreet'>foo</div>\n" +
                "      <div class='addressBlockLine' id='addressDonerCityStateZip'>bar, MD, 11111</div>\n" +
                "    </div>\n" +
                "\n" +
                "  </div>\n";

        LocalDate from = LocalDate.of(2015, 1, 1);
        LocalDate to = LocalDate.of(2015, 2, 2);
        ReportHeader toCheck = new ReportHeader("ministry", AddressBlockTest.sample(), from, to);
        assertEquals(expected, toCheck.asHtmlDivWithId(Id.REPORT_HEADER_ID, Indentation.spaces(2).indent()));

    }
}
