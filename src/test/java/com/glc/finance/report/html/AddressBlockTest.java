package com.glc.finance.report.html;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AddressBlockTest {
    @Test
    public void htmlPrintsAsExpected() {
        String expected = ""
                + "\n<div id='donerAddress'>\n"
                + "  <div class='addressBlockLine' id='addressDonerName'>scott</div>\n"
                + "  <div class='addressBlockLine' id='addressDonerStreet'>foo</div>\n"
                + "  <div class='addressBlockLine' id='addressDonerCityStateZip'>bar, MD, 11111</div>\n"
                + "</div>\n";
        assertEquals(expected, sample().asHtmlDivWithId(Id.DONOR_ADDRESS_BLOCK_ID, Indentation.spaces(2)));
    }

    public static AddressBlock sample() {
        return new AddressBlock("scott", "foo", "bar, MD, 11111");
    }
}
