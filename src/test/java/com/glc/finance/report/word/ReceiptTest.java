package com.glc.finance.report.word;

import glc.finance.apps.Donor;
import glc.finance.apps.Transaction;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.junit.Test;

import com.glc.finance.report.html.AddressBlock;

public class ReceiptTest {

	@Test
	public void testDonation() throws Exception {
		ArrayList<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(new Transaction(LocalDate.of(2015, Month.JULY, 23),
				"Big Bird", "Grace Life Church", "Check", "123",
				new BigDecimal(12.01)));
		transactions.add(new Transaction(LocalDate.of(2015, Month.MAY, 15),
				"Big Bird", "SSM", "Online", "",
				new BigDecimal(5.06)));
		Receipt receipt = new Receipt(new AddressBlock("Big Bird",
				"12345 Sesame Street", "Sesame Place, North Sesame 90210"),
				new Donor("Big Bird", transactions));
		XWPFDocument document = receipt.document();
		document.write(new FileOutputStream(new File("out.docx")));
	}

}
